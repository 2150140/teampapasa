<?php
?>
<!DOCTYPE html>
<html lang="en">
    <!--logo-->
    <link href="../img/RIT.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ReviewIT</title>
    <link rel="stylesheet" href="../css/main.css">

</head>
<body>
<div class="navbar">
	<h1 class="center" style="font-family:Courier New;">My Reviewer</h1>
    </div>
<?php 
	if (isset($_POST['submit'])) {
        $user = $_POST['user'];
		$category = $_POST['category'];
	if($category == "http") {
	echo '
	
	<h1 class="center">Hypertext Transfer Protocol</h1>
	<div class="card">

		<div class="card-item">
			<div class="card-body">
				<form method="POST" action="show_quiz.php">
					<h2 class="card-title center">Multiple Choice</h2>
					<font-size:300%> &nbsp;&nbsp; Details or reviewer for the quiz.</p>
					<div class="input-group">
						<i class="fas fa-user"></i>
						<center><input type="number" name="items" min="5" max="20" placeholder="# Quiz" required><center>
						<span class="line"></span>
					</div>
					<div class="card-footer center">
						<input type="submit" role="button" class="start" name="http_mc" value="Start">
						<input type="hidden" name="user" value="'.$user.'">
					</div>
				</form>
			</div>
		</div>

		<div class="card-item">
			<div class="card-body">
				<h2 class="card-title center">True or False</h2>
				<p class="card-text">Details or reviewer for the quiz.</p>
				<form method="POST" action="show_quiz.php">
					<div class="input-group">
						<i class="fas fa-user"></i>
						<center><input type="number" name="items" min="5" max="20" placeholder="# Quiz" required>
						<span class="line"></span></center>
					</div>
					<div class="card-footer center">
						<input type="submit" role="button" class="start" name="http_tf" value="Start">
						<input type="hidden" name="user" value="'.$user.'">
					</div>
				</form>
			</div>
		</div>
		
		
		<div class="card-item">
			<div class="card-body">
				<h2 class="card-title center">Identification Test</h2>
				<p class="card-text">Details or reviewer for the quiz.</p>
				<form method="POST" action="show_quiz.php">
				<div class="input-group">
                    <i class="fas fa-user"></i>
                    <center><input type="number" name="items" min="5" max="20" placeholder="# Quiz" required>
                    <span class="line"></span></NOcenter>
                </div>
				<div class="card-footer center">
					<input type="submit" role="button" class="start" name="http_id" value="Start">
					<input type="hidden" name="user" value="'.$user.'">
				</div>
				</form>
			</div>
		</div>
        
        <div class="card-item1">
			<div class="card-body1">
				<h2 class="card-title center" name="user" value="'.$user.'">'.$user.'</h2>
				<p class="card-text"></p>
				<div class="card-footer center">
					<a class="start" href="../index.php">Exit</a>
				</div>
			</div>
		</div>
		</div>
	</div>';
	}

	if($category == "html") {
	echo '
	<h1 class="center">HyperText Markup Language</h1>
	<div class="card">
		<div class="card-item">
			<div class="card-body">
				<h2 class="card-title center">Multiple Choice</h2>
				<p class="card-text">Details or reviewer for the quiz.</p>
				<form method="POST" action="show_quiz.php">
				<div class="input-group">
                    <center><i class="fas fa-user"></i>
                    <input type="number" name="items" min="5" max="20" placeholder="# Quiz" required>
                    <span class="line"></span></center>
                </div>
				<div class="card-footer center">
					<input type="submit" role="button" class="start" name="html_mc" value="Start">
					<input type="hidden" name="user" value="'.$user.'">
				</div>
				</form>
			</div>
		</div>

		<div class="card-item">
			<div class="card-body">
				<h2 class="card-title center">True or False</h2>
				<p class="card-text">Details or reviewer for the quiz.</p>
				<form method="POST" action="show_quiz.php">
                <div class="input-group">
                <i class="fas fa-user"></i>
                    <center><input type="number" name="items" min="5" max="30" placeholder="# Quiz" required>
                    <span class="line"></span></center>
                </div>
				<div class="card-footer center">
					<input type="submit" role="button" class="start" name="html_tf" value="Start">
					<input type="hidden" name="user" value="'.$user.'">
				</div>
			</div>
		</div>
		<div class="card-item">
			<div class="card-body">
				<h2 class="card-title center">Identification Test</h2>
				<p class="card-text">Details or reviewer for the quiz.</p>
				<form method="POST" action="show_quiz.php">
				<div class="input-group">
                    <i class="fas fa-user"></i>
                    <center><input type="number" name="items" min="5" max="30" placeholder="# Quiz" required>
                    <span class="line"></span></center>
                </div>              
				<div class="card-footer center">
					<input type="submit" role="button" class="start" name="html_id" value="Start">
					<input type="hidden" name="user" value="'.$user.'">
				</div>
				</form>
			</div>
		</div>
	        <div class="card-item">
			<div class="card-body1">
				<h2 class="card-title center">'.$user.'</h2>
				<input type="hidden" value="'.$user.'" name="user">
				<p class="card-text"></p>
				<div class="card-footer center">
					<a class="start" href="../index.php">Exit</a>
				</div>
			</div>
		</div>
		</div>
	</div>';
	}

	if($category == "css") {
	echo '
	<h1 class="center">Cascading Style Sheets</h1>
	<div class="card">

		<div class="card-item">
			<div class="card-body">
				<h2 class="card-title center">Multiple Choice</h2>
				<p class="card-text">Details or reviewer for the quiz.</p>
				<form method="POST" action="show_quiz.php">
				<div class="input-group">
                    <i class="fas fa-user"></i>
                    <center><input type="number" name="items" min="5" max="30" placeholder="# Quiz" required>
                    <span class="line"></span></center>
                </div>
				<div class="card-footer center">
					<input type="submit" role="button" class="start" name="css_mc" value="Start">
					<input type="hidden" name="user" value="'.$user.'">
				</div>
				</form>
			</div>
		</div>

		<div class="card-item">
			<div class="card-body">
				<h2 class="card-title center">True or False</h2>
				<p class="card-text">Details or reviewer for the quiz.</p>
				<form method="POST" action="show_quiz.php">
				<div class="input-group">
                    <i class="fas fa-user"></i>
                    <center><input type="number" name="items" min="5" max="30" placeholder="# Quiz" required>
                    <span class="line"></span></center>
                </div>
				<div class="card-footer center">
					<input type="submit" role="button" class="start" name="css_tf" value="Start">
					<input type="hidden" name="user" value="'.$user.'">
				</div>
				</form>
			</div>
		</div>
		
		
		<div class="card-item">
			<div class="card-body">
				<h2 class="card-title center">Identification Test</h2>
				<p class="card-text">Details or reviewer for the quiz.</p>
				<form method="POST" action="show_quiz.php">
				<div class="input-group">
                    <i class="fas fa-user"></i>
                    <center><input type="number" name="items" min="5" max="30" placeholder="# Quiz" required>
                    <span class="line"></span></center>
                </div>
				<div class="card-footer center">
					<input type="submit" role="button" class="start" name="css_id" value="Start">
					<input type="hidden" name="user" value="'.$user.'">
				</div>
				</form>
			</div>
		</div>
	        <div class="card-item">
			<div class="card-body1">
				<h2 class="card-title center">'.$user.'</h2>
				<input type="hidden" value="'.$user.'" name="user">
				<p class="card-text"></p>
				<div class="card-footer center">
					<a class="start" href="../index.php">Exit</a>
				</div>
			</div>
		</div>
		</div>
	</div>';
	}

	if($category == "js") {
	echo '
	<div class="card">
		<div class="card-item">
			<div class="card-body">
				<h2 class="card-title center">Multiple Choice</h2>
				<p class="card-text">Details or reviewer for the quiz.</p>
				<form method="POST" action="show_quiz.php">
				<div class="input-group">
                    <i class="fas fa-user"></i>
                    <center><input type="number" name="items" min="5" max="30" placeholder="# Quiz" required>
                    <span class="line"></span></center>
                </div>
				<div class="card-footer center">
					<input type="submit" role="button" class="start" name="js_mc" value="Start">
					<input type="hidden" name="user" value="'.$user.'">
				</div>
				</form>
			</div>
		</div>

		<div class="card-item">
			<div class="card-body">
				<h2 class="card-title center">True or False</h2>
				<p class="card-text">Details or reviewer for the quiz.</p>
				<form method="POST" action="show_quiz.php">
				<div class="input-group">
                    <i class="fas fa-user"></i>
					
                    <center><input type="number" name="items" min="5" max="30" placeholder="# Quiz" required>
                    <span class="line"></span></center>
                </div>
				<div class="card-footer center">
					<input type="submit" role="button" class="start" name="js_tf" value="Start">
					<input type="hidden" name="user" value="'.$user.'">
				</div>
				</form>
			</div>
		</div>
		
		<div class="card-item">
			<div class="card-body">
				<h2 class="card-title center">Identification Test</h2>
				<p class="card-text">Details or reviewer for the quiz.</p>
				<form method="POST" action="show_quiz.php">
				<div class="input-group">
                    <i class="fas fa-user"></i>
                    <center><input type="number" name="items" min="5" max="30" placeholder="# Quiz" required>
                    <span class="line"></span></center>
                </div>
				<div class="card-footer center">
					<input type="submit" role="button" class="start" name="js_id" value="Start">
					<input type="hidden" name="user" value="'.$user.'">
				</div>
				</form>
			</div>
		</div>
        	        <div class="card-item">
			<div class="card-body1">
				<h2 class="card-title center">'.$user.'</h2>
				<input type="hidden" value="'.$user.'" name="user">
				<p class="card-text"></p>
				<div class="card-footer center">
					<a class="start" href="../index.php">Exit</a>
				</div>
			</div>
		</div>

		</div>
	</div>';
	}

	
	}
	?>
</form>
</body>
</html>
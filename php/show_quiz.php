<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ReviewIT</title>
    <link rel="stylesheet" href="../css/main.css">
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
</head>
<body>
<div class="navbar">
    <Center><img src ="../img/RIT.png" width="100" height="100" onclick="goBack()"></Center>
    <script>
function goBack() {
  window.history.back();
}
</script>
</div>
<?php 
        include 'database.php';
		// Multiple choice
        if(isset($_POST['js_mc'])) {
            $name = $_POST['user'];	
            $query = "SELECT * FROM js_mc order by rand() limit ".$_POST['items'];
            $result = $conn->query($query);
			$count = 1;
		
			echo '<input type="hidden" id="number" value="'.$_POST['items'].'">';
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="username" value="'.$name.'">'.$name.'</h2>';
			echo '<h3 id="scorelbl">Score: </h3>'; 
			echo '<form id="savescore">';
			echo '<input type="hidden" name="thescore" value="" id="thescore">';
			echo '<input type="hidden" name="thename" value="'.$name.'" id="thename">';
			echo '<input type="submit"  id="savebutton">';
			echo '</form>';
			echo '<input type="hidden" value="" id="thescore" name="thescore">';
			echo '<div></div>';
			echo '<label>You have '.$_POST['items'].' minutes to answer</label>';
			echo '<div></div>';
			echo '<label>Time Lapsed: <div class="clock"></div></label>';
			echo '<br>';
			echo '<b id="time"></b>';
            echo '</div>';
            echo '</div>';
			echo '</form>';
			
            if($result->num_rows > 0) {
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $temp_answer1 = $data['answer1'];
                    $temp_answer2 = $data['answer2'];
                    $temp_answer3 = $data['answer3'];
                    $correct_answer = $data['correct_answer'];
					$correct_answer_text = htmlentities($data['correct_answer_text']);
                    $answer1 = htmlentities($temp_answer1);
                    $answer2 = htmlentities($temp_answer2);
                    $answer3 = htmlentities($temp_answer3);
					
					
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$count.'.&nbsp;&nbsp;'.$question.'</b></h4> ';

                    if (isset($answer1)) {
                    echo '<input type="radio" class="uanswer" name="'.$id.'" id="1" value="'.$correct_answer_text.'">'.$answer1.'';
                    }

                    if (isset($answer1)) {
                    echo '<input type="radio" class="uanswer" name="'.$id.'" id="2" value="'.$correct_answer_text.'">'.$answer2.'';
                    }

                    if (isset($answer1)) {
                    echo '<input type="radio" class="uanswer" name="'.$id.'" id="3" value="'.$correct_answer_text.'">'.$answer3.'';
                    }

                    echo '<br>';
                    echo '<br>';

                    echo '<label class="ans"><b>Correct Answer:</b> '.$correct_answer_text.'</label>';
					
                    echo '<input type="hidden" class="correct" value="'.$correct_answer_text.'" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
					
					$count++;
                }
            }
            echo '<input type="hidden"  name="username" value="'.$name.'">';
            echo '<input type="submit" id="mc" class="submit" name="submit_quiz" onclick="check_a()" value="Submit Quiz">';
			echo '<input type="hidden" id="score" value="0">';
			
			
        } else  if(isset($_POST['html_mc'])) {
            $name = $_POST['user'];
            $query = "SELECT * FROM html_mc order by rand() limit ".$_POST['items'];
            $result = $conn->query($query);
			$count = 1;
		
			echo '<input type="hidden" id="number" value="'.$_POST['items'].'">';
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="username" value="'.$name.'">'.$name.'</h2>';
			echo '<h3 id="scorelbl">Score: </h3>'; 
			echo '<form id="savescore">';
			echo '<input type="hidden" name="thescore" value="" id="thescore">';
			echo '<input type="hidden" name="thename" value="'.$name.'" id="thename">';
				echo '<input type="submit"  id="savebutton">';
			echo '</form>';
			echo '<input type="hidden" value="" id="thescore" name="thescore">';
			echo '<div></div>';
			echo '<label>You have '.$_POST['items'].' minutes to answer</label>';
			echo '<div></div>';
			echo '<label>Time Lapsed: <div class="clock"></div></label>';

            echo '</div>';
            echo '</div>';
			echo '</form>';
			
            if($result->num_rows > 0) {
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $temp_answer1 = $data['answer1'];
                    $temp_answer2 = $data['answer2'];
                    $temp_answer3 = $data['answer3'];
                    $correct_answer = $data['correct_answer'];
					$correct_answer_text = htmlentities($data['correct_answer_text']);
                    $answer1 = htmlentities($temp_answer1);
                    $answer2 = htmlentities($temp_answer2);
                    $answer3 = htmlentities($temp_answer3);
					
					
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$count.'.&nbsp;&nbsp;'.$question.'</b></h4> ';

                    if (isset($answer1)) {
                    echo '<input type="radio" class="uanswer" name="'.$id.'" id="1" value="'.$correct_answer_text.'">'.$answer1.'';
                    }

                    if (isset($answer1)) {
                    echo '<input type="radio" class="uanswer" name="'.$id.'" id="2" value="'.$correct_answer_text.'">'.$answer2.'';
                    }

                    if (isset($answer1)) {
                    echo '<input type="radio" class="uanswer" name="'.$id.'" id="3" value="'.$correct_answer_text.'">'.$answer3.'';
                    }

                    echo '<br>';
                    echo '<br>';

                    echo '<label class="ans"><b>Correct Answer:</b> '.$correct_answer_text.'</label>';
					
                    echo '<input type="hidden" class="correct" value="'.$correct_answer_text.'" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
					
					$count++;
                }
            }
            echo '<input type="hidden"  name="username" value="'.$name.'">';
            echo '<input type="submit" id="mc" class="submit" name="submit_quiz" onclick="check_a()" value="Submit Quiz">';
			echo '<input type="hidden" id="score" value="0">';
			
			
        } else  if(isset($_POST['http_mc'])) {
            $name = $_POST['user'];
            $query = "SELECT * FROM html_mc order by rand() limit ".$_POST['items'];
            $result = $conn->query($query);
			$count = 1;

			echo '<input type="hidden" id="number" value="'.$_POST['items'].'">';
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="username" value="'.$name.'">'.$name.'</h2>';
			echo '<h3 id="scorelbl">Score: </h3>'; 
			echo '<form id="savescore">';
			echo '<input type="hidden" name="thescore" value="" id="thescore">';
			echo '<input type="hidden" name="thename" value="'.$name.'" id="thename">';
			echo '<input type="submit"  id="savebutton">';
			echo '</form>';
			echo '<input type="hidden" value="" id="thescore" name="thescore">';
			echo '<div></div>';
			echo '<label>You have '.$_POST['items'].' minutes to answer</label>';
			echo '<div></div>';
			echo '<label>Time Lapsed: <div class="clock"></div></label>';
            echo '</div>';
            echo '</div>';
			echo '</form>';
			
            if($result->num_rows > 0) {
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $temp_answer1 = $data['answer1'];
                    $temp_answer2 = $data['answer2'];
                    $temp_answer3 = $data['answer3'];
                    $correct_answer = $data['correct_answer'];
					$correct_answer_text = htmlentities($data['correct_answer_text']);
                    $answer1 = htmlentities($temp_answer1);
                    $answer2 = htmlentities($temp_answer2);
                    $answer3 = htmlentities($temp_answer3);
					
					
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$count.'.&nbsp;&nbsp;'.$question.'</b></h4> ';

                    if (isset($answer1)) {
                    echo '<input type="radio" class="uanswer" name="'.$id.'" id="1" value="'.$correct_answer_text.'">'.$answer1.'';
                    }

                    if (isset($answer1)) {
                    echo '<input type="radio" class="uanswer" name="'.$id.'" id="2" value="'.$correct_answer_text.'">'.$answer2.'';
                    }

                    if (isset($answer1)) {
                    echo '<input type="radio" class="uanswer" name="'.$id.'" id="3" value="'.$correct_answer_text.'">'.$answer3.'';
                    }

                    echo '<br>';
                    echo '<br>';

                    echo '<label class="ans"><b>Correct Answer:</b> '.$correct_answer_text.'</label>';
					
                    echo '<input type="hidden" class="correct" value="'.$correct_answer_text.'" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
					
					$count++;
                }
            }
            echo '<input type="hidden"  name="username" value="'.$name.'">';
            echo '<input type="submit" id="mc" class="submit" name="submit_quiz" onclick="check_a()" value="Submit Quiz">';
			echo '<input type="hidden" id="score" value="0">';
			
			
        } else  if(isset($_POST['css_mc'])) {
            $name = $_POST['user'];
            $query = "SELECT * FROM html_mc order by rand() limit ".$_POST['items'];
            $result = $conn->query($query);
			$count = 1;

			echo '<input type="hidden" id="number" value="'.$_POST['items'].'">';
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="username" value="'.$name.'">'.$name.'</h2>';			
			echo '<h3 id="scorelbl">Score: </h3>'; 
			echo '<form id="savescore">';
			echo '<input type="hidden" name="thescore" value="" id="thescore">';
			echo '<input type="hidden" name="thename" value="'.$name.'" id="thename">';
			echo '<input type="submit"  id="savebutton">';
			echo '</form>';
			echo '<input type="hidden" value="" id="thescore" name="thescore">';
			echo '<div></div>';
			echo '<label>You have '.$_POST['items'].' minutes to answer</label>';
			echo '<div></div>';
			echo '<label>Time Lapsed: <div class="clock"></div></label>';
            echo '</div>';
            echo '</div>';
			echo '</form>';
			
            if($result->num_rows > 0) {
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $temp_answer1 = $data['answer1'];
                    $temp_answer2 = $data['answer2'];
                    $temp_answer3 = $data['answer3'];
                    $correct_answer = $data['correct_answer'];
					$correct_answer_text = htmlentities($data['correct_answer_text']);
                    $answer1 = htmlentities($temp_answer1);
                    $answer2 = htmlentities($temp_answer2);
                    $answer3 = htmlentities($temp_answer3);
					
					
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$count.'.&nbsp;&nbsp;'.$question.'</b></h4> ';

                    if (isset($answer1)) {
                    echo '<input type="radio" class="uanswer" name="'.$id.'" id="1" value="'.$correct_answer_text.'">'.$answer1.'';
                    }

                    if (isset($answer1)) {
                    echo '<input type="radio" class="uanswer" name="'.$id.'" id="2" value="'.$correct_answer_text.'">'.$answer2.'';
                    }

                    if (isset($answer1)) {
                    echo '<input type="radio" class="uanswer" name="'.$id.'" id="3" value="'.$correct_answer_text.'">'.$answer3.'';
                    }

                    echo '<br>';
                    echo '<br>';

                    echo '<label class="ans"><b>Correct Answer:</b> '.$correct_answer_text.'</label>';
					
                    echo '<input type="hidden" class="correct" value="'.$correct_answer_text.'" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
					
					$count++;
                }
            }
            echo '<input type="hidden"  name="username" value="'.$name.'">';
            echo '<input type="submit" id="mc" class="submit" name="submit_quiz" onclick="check_a()" value="Submit Quiz">';
			echo '<input type="hidden" id="score" value="0">';
			
			
        }
		
		// aydentifikeyshon
		else if(isset($_POST['js_id'])) {
            $name = $_POST['user'];
            $query = "SELECT * FROM js_id order by rand() limit ".$_POST['items'];
            $result = $conn->query($query);
			$count = 1;

			echo '<input type="hidden" id="number" value="'.$_POST['items'].'">';
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="username" value="'.$name.'">'.$name.'</h2>';
			echo '<h3 id="scorelbl">Score: </h3>'; 
			echo '<form id="savescore">';
			echo '<input type="hidden" name="thescore" value="" id="thescore">';
			echo '<input type="hidden" name="thename" value="'.$name.'" id="thename">';
			echo '<input type="submit"  id="savebutton">';
			echo '</form>';
			echo '<input type="hidden" value="" id="thescore">';
			echo '<div></div>';
			echo '<label>You have '.$_POST['items'].' minutes to answer</label>';
			echo '<div></div>';
			echo '<label>Time Lapsed: <div class="clock"></div></label>';
            echo '</div>';
            echo '</div>';
			echo '</form>';
            if($result->num_rows > 0) {
				
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $answer = htmlentities($data['answer']);
					
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$count.'.&nbsp;&nbsp;'.$question.'</b></h4> ';

                    echo '<input type="input" class="input" value="" name="'.$id.'" id="1" >';


                    echo '<br>';
                    echo '<br>';

                    echo '<label class="ans"><b>Correct Answer:</b> '.$answer.'</label>';
					
                    echo '<input type="hidden" class="correct" value="'.$answer.'" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
					
					$count++;
                }
            }
            echo '<input type="hidden"  name="username" value="'.$name.'">';
            echo '<input type="submit" id="id" class="submit" name="submit_quiz" onclick="check_b()" value="Submit Quiz">';
			echo '<input type="hidden" id="score" value="0">';
			
			
        } else if(isset($_POST['html_id'])) {
            $name = $_POST['user'];
            $query = "SELECT * FROM html_id order by rand() limit ".$_POST['items'];
            $result = $conn->query($query);
			$count = 1;
			
	
			echo '<input type="hidden" id="number" value="'.$_POST['items'].'">';
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="username" value="'.$name.'">'.$name.'</h2>';
			echo '<h3 id="scorelbl">Score: </h3>'; 
			echo '<form id="savescore">';
			echo '<input type="hidden" name="thescore" value="" id="thescore">';
			echo '<input type="hidden" name="thename" value="'.$name.'" id="thename">';
			echo '<input type="submit"  id="savebutton">';
			echo '</form>';
			echo '<input type="hidden" value="" id="thescore">';
			echo '<div></div>';
			echo '<label>You have '.$_POST['items'].' minutes to answer</label>';
			echo '<div></div>';
			echo '<label>Time Lapsed: <div class="clock"></div></label>';
            echo '</div>';
            echo '</div>';
			echo '</form>';
            if($result->num_rows > 0) {
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $answer = htmlentities($data['answer']);
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$count.'.&nbsp;&nbsp;'.$question.'</b></h4> ';

                    echo '<input type="input" class="input" name="'.$id.'" id="1" >';


                    echo '<br>';
                    echo '<br>';

                    echo '<label class="ans"><b>Correct Answer:</b> '.$answer.'</label>';
					
                    echo '<input type="hidden" class="correct" value="'.$answer.'" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
					
					$count++;
                }
            }
            echo '<input type="hidden"  name="username" value="'.$name.'">';
            echo '<input type="submit" id="id" class="submit" name="submit_quiz" onclick="check_b()" value="Submit Quiz">';
			echo '<input type="hidden" id="score" value="0">';
			
			
			
        } else if(isset($_POST['http_id'])) {
            $name = $_POST['user'];
            $query = "SELECT * FROM http_id order by rand() limit ".$_POST['items'];
            $result = $conn->query($query);
			$count = 1;

			echo '<input type="hidden" id="number" value="'.$_POST['items'].'">';
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="username" value="'.$name.'">'.$name.'</h2>';
			echo '<h3 id="scorelbl">Score: </h3>'; 
			echo '<form id="savescore">';
			echo '<input type="hidden" name="thescore" value="" id="thescore">';
			echo '<input type="hidden" name="thescore" value="'.$name.'" id="thename">';
			echo '<input type="submit"  id="savebutton">';
			echo '</form>';
			echo '<input type="hidden" value="" id="thescore">';
			echo '<div></div>';
			echo '<label>You have '.$_POST['items'].' minutes to answer</label>';
			echo '<div></div>';
			echo '<label>Time Lapsed: <div class="clock"></div></label>';
            echo '</div>';
            echo '</div>';
			echo '</form>';
            if($result->num_rows > 0) {
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $answer = htmlentities($data['answer']);
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$count.'.&nbsp;&nbsp;'.$question.'</b></h4> ';

                    echo '<input type="input" class="input" name="'.$id.'" id="1" >';


                    echo '<br>';
                    echo '<br>';

                    echo '<label class="ans"><b>Correct Answer:</b> '.$answer.'</label>';
					
                    echo '<input type="hidden" class="correct" value="'.$answer.'" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
					
					$count++;
					
                }
            }
            echo '<input type="hidden"  name="username" value="'.$name.'">';
            echo '<input type="submit" id="id" class="submit" name="submit_quiz" onclick="check_b()" value="Submit Quiz">';
			echo '<input type="hidden" id="score" value="0">';
			
			
			
        } else if(isset($_POST['css_id'])) {
            $name = $_POST['user'];
            $query = "SELECT * FROM css_id order by rand() limit ".$_POST['items'];
            $result = $conn->query($query);
			$count = 1;

			echo '<input type="hidden" id="number" value="'.$_POST['items'].'">';
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="username" value="'.$name.'">'.$name.'</h2>';
			echo '<h3 id="scorelbl">Score: </h3>'; 
			echo '<form id="savescore">';
			echo '<input type="hidden" name="thescore" value="" id="thescore">';
			echo '<input type="hidden" name="thename" value="'.$name.'" id="thename">';
			echo '<input type="submit"  id="savebutton">';
			echo '</form>';
			echo '<input type="hidden" value="" id="thescore">';
			echo '<div></div>';
			echo '<label>You have '.$_POST['items'].' minutes to answer</label>';
			echo '<div></div>';
			echo '<label>Time Lapsed: <div class="clock"></div></label>';
            echo '</div>';
            echo '</div>';
			echo '</form>';
            if($result->num_rows > 0) {
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $answer = htmlentities($data['answer']);
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$count.'.&nbsp;&nbsp;'.$question.'</b></h4> ';

                    echo '<input type="input" class="input" name="'.$id.'" id="1" >';


                    echo '<br>';
                    echo '<br>';

                    echo '<label class="ans"><b>Correct Answer:</b> '.$answer.'</label>';
					
                    echo '<input type="hidden" class="correct" value="'.$answer.'" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
					
					$count++;
					
                }
            }
            echo '<input type="hidden"  name="username" value="'.$name.'">';
            echo '<input type="submit" id="id" class="submit" name="submit_quiz" onclick="check_b()" value="Submit Quiz">';
			echo '<input type="hidden" id="score" value="0">';
        } 
		
		
		// tru or fols
		else if(isset($_POST['js_tf'])) {
            $name = $_POST['user'];
            $query = "SELECT * FROM js_tf order by rand() limit ".$_POST['items'];
            $result = $conn->query($query);
			$count = 1;

			echo '<input type="hidden" id="number" value="'.$_POST['items'].'">';
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="username" value="'.$name.'">'.$name.'</h2>';
			echo '<h3 id="scorelbl">Score: </h3>'; 
			echo '<form id="savescore">';
			echo '<input type="hidden" name="thescore" value="" id="thescore">';
			echo '<input type="hidden" name="thename" value="'.$name.'" id="thename">';
			echo '<input type="submit" id="savebutton">';
			echo '</form>';
			echo '<input type="hidden" value="" id="thescore">';
			echo '<div></div>';
			echo '<label>You have '.$_POST['items'].' minutes to answer</label>';
			echo '<div></div>';
			echo '<div><span><label>Time Lapsed:</label> <div class="clock"></div></span></div>';
            echo '</div>';
            echo '</div>';
			echo '</form>';
            if($result->num_rows > 0) {
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $answer = htmlentities($data['answer']);
                    
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$count.'.&nbsp;&nbsp;'.$question.'?</b></h4> ';

                  
                    echo '<input type="radio" class="ianswer" name="'.$id.'" id="true" value="true">True';
                  
                    echo '<input type="radio" class="ianswer" name="'.$id.'" id="false" value="false">False';
                    

                    echo '<br>';
                    echo '<br>';

                    echo '<label class="ans"><b>Correct Answer:</b> '.$answer.'</label>';
					
                    echo '<input type="hidden" class="correct" value="'.$answer.'" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
					
					$count++;
					
					
                }
            }
            echo '<input type="hidden"  name="username" value="'.$name.'">';
            echo '<input type="submit" id="tf" class="submit" name="submit_quiz" onclick="check_a()" value="Submit Quiz">';
			echo '<input type="hidden" id="score" value="0">';
			
        } else if(isset($_POST['html_tf'])) {
            $name = $_POST['user'];
            $query = "SELECT * FROM html_tf order by rand() limit ".$_POST['items'];
            $result = $conn->query($query);
			$count = 1;

			echo '<input type="hidden" id="number" value="'.$_POST['items'].'">';
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="username" value="'.$name.'">'.$name.'</h2>';
			echo '<h3 id="scorelbl">Score: </h3>';  
			echo '<form id="savescore">';
			echo '<input type="hidden" name="thescore" value="" id="thescore">';
			echo '<input type="hidden" name="thename" value="'.$name.'" id="thename">';
			echo '<input type="submit"  id="savebutton">';
			echo '</form>';
			echo '<input type="hidden" value="" id="thescore">';
			echo '<div></div>';
			echo '<label>You have '.$_POST['items'].' minutes to answer</label>';
			echo '<div></div>';
			echo '<label>Time Lapsed: <div class="clock"></div></label>';
            echo '</div>';
            echo '</div>';
			echo '</form>';
            if($result->num_rows > 0) {
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $answer = htmlentities($data['answer']);
					
                    
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$count.'.&nbsp;&nbsp;'.$question.'?</b></h4> ';

                  
                    echo '<input type="radio" class="ianswer" name="'.$id.'" id="true" value="true">True';
                  
                    echo '<input type="radio" class="ianswer" name="'.$id.'" id="false" value="false">False';
                    

                    echo '<br>';
                    echo '<br>';

                    echo '<label class="ans"><b>Correct Answer:</b> '.$answer.'</label>';
					
                    echo '<input type="hidden" class="correct" value="'.$answer.'" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
					
					$count++;
                }
            }
            echo '<input type="hidden"  name="username" value="'.$name.'">';
            echo '<input type="submit" id="id" class="submit" name="submit_quiz" onclick="check_a()" value="Submit Quiz">';
			echo '<input type="hidden" id="score" value="0">';
        } else if(isset($_POST['css_tf'])) {
            $name = $_POST['user'];
            $query = "SELECT * FROM css_tf order by rand() limit ".$_POST['items'];
            $result = $conn->query($query);
			$count = 1;

			echo '<input type="hidden" id="number" value="'.$_POST['items'].'">';
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="username" value="'.$name.'">'.$name.'</h2>';
			echo '<h3 id="scorelbl">Score: </h3>'; 
						echo '<form id="savescore">';
			echo '<input type="hidden" name="thescore" value="" id="thescore">';
			echo '<input type="hidden" name="thename" value="'.$name.'" id="thename">';
			echo '<input type="submit"  id="savebutton">';
			echo '</form>';
			echo '<input type="hidden" value="" id="thescore">';
			echo '<div></div>';
			echo '<label>You have '.$_POST['items'].' minutes to answer</label>';
			echo '<div></div>';
			echo '<label>Time Lapsed: <div class="clock"></div></label>';
            echo '</div>';
            echo '</div>';
			echo '</form>';
            if($result->num_rows > 0) {
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $answer = htmlentities($data['answer']);
				
                    
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$count.'.&nbsp;&nbsp;'.$question.'?</b></h4> ';

                  
                    echo '<input type="radio" class="ianswer" name="'.$id.'" id="true" value="true">True';
                  
                    echo '<input type="radio" class="ianswer" name="'.$id.'" id="false" value="false">False';
                    

                    echo '<br>';
                    echo '<br>';

                    echo '<label class="ans"><b>Correct Answer:</b> '.$answer.'</label>';
					
                    echo '<input type="hidden" class="correct" value="'.$answer.'" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
					
					$count++;
					
                }
            }
            echo '<input type="hidden"  name="username" value="'.$name.'">';
            echo '<input type="submit" id="id" class="submit" name="submit_quiz" onclick="check_a()" value="Submit Quiz">';
			echo '<input type="hidden" id="score" value="0">';
			
        }  else if(isset($_POST['http_tf'])) {
            $name = $_POST['user'];
            $query = "SELECT * FROM http_tf order by rand() limit ".$_POST['items'];
            $result = $conn->query($query);
			$count = 1;

			echo '<input type="hidden" id="number" value="'.$_POST['items'].'">';
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="username" value="'.$name.'">'.$name.'</h2>';
			echo '<h3 id="scorelbl">Score: </h3>'; 
			echo '<form id="savescore">';
			echo '<input type="hidden" name="thescore" value="" id="thescore">';
			echo '<input type="hidden" name="thename" value="'.$name.'" id="thename">';
			echo '<input type="submit" id="savebutton">';
			echo '</form>';
			echo '<div></div>';
			echo '<label>You have '.$_POST['items'].' minutes to answer</label>';
			echo '<div></div>';
			echo '<label>Time Lapsed: <div class="clock"></div></label>';
            echo '</div>';
            echo '</div>';
			echo '</form>';
            if($result->num_rows > 0) {
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $answer = htmlentities($data['answer']);
                    
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$count.'.&nbsp;&nbsp;'.$question.'?</b></h4> ';

                  
                    echo '<input type="radio" class="ianswer" name="'.$id.'" id="true" value="true">True';
                  
                    echo '<input type="radio" class="ianswer" name="'.$id.'" id="false" value="false">False';
                    

                    echo '<br>';
                    echo '<br>';

                    echo '<label class="ans"><b>Correct Answer:</b> '.$answer.'</label>';
					
                    echo '<input type="hidden" class="correct" value="'.$answer.'" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
					
					$count++;
                }
            }
            echo '<input type="hidden"  name="username" value="'.$name.'">';
            echo '<input type="submit" id="id" class="submit" name="submit_quiz" onclick="check_a()" value="Submit Quiz">';
			echo '<input type="hidden" id="score" value="0">';
        }
        
    ?>
   <script defer> 
   
		$(document).ready(function(){
			 function get_elapsed_time_string(total_seconds) {
			  function pretty_time_string(num) {
				return ( num < 10 ? "0" : "" ) + num;
			  }

			  var hours = Math.floor(total_seconds / 3600);
			  total_seconds = total_seconds % 3600;

			  var minutes = Math.floor(total_seconds / 60);
			  total_seconds = total_seconds % 60;

			  var seconds = Math.floor(total_seconds);

			  // Pad the minutes and seconds with leading zeros, if required
			  hours = pretty_time_string(hours);
			  minutes = pretty_time_string(minutes);
			  seconds = pretty_time_string(seconds);

			  // Compose the string for display
			  var currentTimeString = hours + ":" + minutes + ":" + seconds;

			  return currentTimeString;
			}

			var elapsed_seconds = 0;
			setInterval(function() {
			  elapsed_seconds = elapsed_seconds + 1;
			  $('.clock').text(get_elapsed_time_string(elapsed_seconds));
			}, 1000);
		});
	   
	   
        setTimeout(function(){
			alert('Time is up!');
			$('#mc').click();
			$('#tf').click();
			$('#id').click();
		},60000 * $('#number').val());
		
		$('.ans').hide();
			$('#savebutton').hide();
		
		
   </script> 

<script defer>
	$('#mc').on('click',function(){
		var score = 0;
		var answers = $('input[class="uanswer"]:checked');
		var correct = $('input[class="correct"]');
		for(i = 0; i!=answers.length;i++){
			for(j = 0; j!=correct.length;j++){
				if(answers[i].value == correct[j].value ){
					score++;
				}
			}
		}
			$('#mc').hide();
			$('.clock').hide();
			$('#scorelbl').text('Score: ' + score);
			$('.ans').show();
			$('#savebutton').show();
		
	});
	
	$('#tf').on('click',function(){
		
		var answers = $('input[class="ianswer"]:checked');
		var correct = $('.correct');
		
		
		var over = 0;
		
		var correctTrue = 0;
		var correctFalse = 0;
		var answerTrue = 0;
		var answerFalse = 0;
		
	
		
		
		for(i = 0; i!=answers.length;i++){
			if(answers[i].value == 'true'){
				answerTrue++;	
			} else
			if(answers[i].value == 'false'){
				answerFalse++;
			}
		}
		
		for(i = 0; i!=answers.length;i++){
			console.log(answers[i].value);
		}
		
		for(i = 0; i!=correct.length;i++){
			if(correct[i].value == 'true'){
				correctTrue++;	
			} else
			if(correct[i].value == 'false'){
				correctFalse++;
			}
			
			over++;
		}
		
				for(i = 0; i!=correct.length;i++){
			console.log(correct[i].value);
		}
		
		var getTrue = correctTrue - answerTrue;
		var getFalse = correctFalse - answerFalse;
		
		if(getTrue <= 0){
			getTrue = 0;
		} 
		
		if(getFalse <= 0){
			getFalse = 0;
		} 
		
		var total = over - (getTrue + getFalse);
		
			$('#tf').hide();
			$('.clock').hide();
			$('#scorelbl').text('Score: ' + total);
			$('.ans').show();
			$('#savebutton').show();
		
	});
	
	$('#id').on('click',function(){
		var score = 0;
		var answers = $('.input');
		var correct = $('.correct');
		console.log(correct[0].value);
		
		for(i = 0; i!=correct.length;i++){
			var x = correct[i].value;
			var y = answers[i].value;
			if(x.toLowerCase() == y.toLowerCase()){
				score++;
			}
		}
		
		$('#id').hide();
		$('.clock').hide();
		$('#scorelbl').text('Score: ' + score);
		$('.ans').show();
		$('#savebutton').show();
	});
</script>

<script>
$(document).ready(function(){
    $('#savescore').on('submit', function(e){
        e.preventDefault();

        $.ajax({
            type: "post",
            url: "save_score.php",
            data: $('#savescore').serialize(),
            success: function (response) {
                console.log(response);
                
                alert('Your score have been saved');
                
            },
            error: function(error){
                console.log(error);
                alert('Your score is not saved');
            }
        });
    });
});
</script>
</body>
</html>
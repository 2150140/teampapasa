<?php include 'database.php';
    $query = "SELECT id, question, answer1, answer2, answer3, correct_answer FROM js_mc";
    $result = $conn->query($query);
    $answers = implode("", $_POST);
    $correct = 0;
    $incorrect = 0;
    $blank = 0;

    while($data = $result->fetch_array(MYSQLI_ASSOC)) {
        if($data['correct_answer'] == $_POST[$data['id']]) {
            $correct++;
        } elseif($_POST[$data['id']] == "no_answer") {
            $blank++;
        } else {
            $incorrect++;
        }
    }

    $dump = array();
    $dump['correct'] = $correct;
    $dump['incorrect'] = $incorrect;
    $dump['blank'] = $blank;

    $total = $dump['correct'] + $dump['incorrect'] + $dump['blank'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MyQuiz</title>
    <link rel="stylesheet" href="../../css/main.css">
</head>
<body>
<div class="navbar">
	<h3 class="center">MyQuiz</h3>
</div>
    <?php 
        include 'database.php';
        if(isset($_POST['submit_quiz'])) {
            $query = "SELECT * FROM js_mc";
            $result = $conn->query($query);
            $username = $_POST['username'];
            echo '<div class="profile-card">';
            echo '<div class="profile-container">';
            echo '<h2 class="profile-title center" name="name">'.$username.'</h2>';
            echo 'Score: '.$dump['correct'].'/'.$total; 
            echo '</div>';
            echo '</div>';
            if($result->num_rows > 0) {
                while($data = mysqli_fetch_assoc($result)) {
                    $id = $data['id'];
                    $question = $data['question'];
                    $temp_answer1 = $data['answer1'];
                    $temp_answer2 = $data['answer2'];
                    $temp_answer3 = $data['answer3'];
                    $temp_correct_answer_text = $data['correct_answer_text'];
                    $correct_answer = $data['correct_answer'];

                    $answer1 = htmlentities($temp_answer1);
                    $answer2 = htmlentities($temp_answer2);
                    $answer3 = htmlentities($temp_answer3);
                    $correct_answer_text = htmlentities($temp_correct_answer_text);
                    echo '<div class="quiz-card">';
                    echo '<div class="quiz-container">';
                    echo '<h4><b>'.$id.'.&nbsp;&nbsp;'.$question.'</b></h4> ';

                    if (isset($answer1)) {
                    echo '<input type="radio" name="'.$id.'" id="" value="1">'.$answer1.'';
                    }

                    if (isset($answer1)) {
                    echo '<input type="radio" name="'.$id.'" id="" value="2">'.$answer2.'';
                    }

                    if (isset($answer1)) {
                    echo '<input type="radio" name="'.$id.'" id="" value="3">'.$answer3.'';
                    }
                    echo '<br>';
                    echo '<br>';

                    echo '<b>Correct Answer:</b> '.$correct_answer_text;

                    echo '<input type="radio" checked="checked" style="display: none !important" value="no_answer" name="'.$id.'">';
                    echo '</div>';
                    echo '</div>';
                }
            }
            echo '<input type="submit" class="submit" name="quit_quiz" value="Quit Quiz">';
        }
        
    ?>

</body>
</html>
